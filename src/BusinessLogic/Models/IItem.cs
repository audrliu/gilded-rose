﻿namespace GildedRose.BusinessLogic.Models
{
    public interface IItem
    {
        string Name { get; }
        int SellIn { get; }
        int Quality { get; }

        /// <summary>
        /// Ages inventory by one day (updates SellIn and Quality properties)
        /// </summary>
        void Update();
    }
}
