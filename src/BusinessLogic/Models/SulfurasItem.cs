﻿namespace GildedRose.BusinessLogic.Models
{
    public class SulfurasItem : DefaultItem
    {
        //"Sulfuras", being a legendary item, never has to be sold or decreases in Quality
        private const int SulfurasQuality = 80;

        /// <summary>
        /// Legendary item. Has to be never sold or decrease in Quality
        /// </summary>
        /// <param name="name"></param>
        /// <param name="sellIn"></param>
        public SulfurasItem(string name, int sellIn) : base(name, sellIn, 0)
        {
            Quality = SulfurasQuality;
        }

        public override void Update()
        {
            // do nothing
        }
    }
}
