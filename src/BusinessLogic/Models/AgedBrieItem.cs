﻿namespace GildedRose.BusinessLogic.Models
{
    public class AgedBrieItem : DefaultItem
    {
        public AgedBrieItem(string name, int sellIn, int quality) : base(name, sellIn, quality) 
        {
        }

        public override void Update()
        {
            SellIn--;

            if (Quality == MaxQuality)
            {
                // Can't get better any more. We are done here.
                return;
            }

            Quality++;
            if (SellIn < 0)
            {
                Quality++;
            }

            ValidateAndFixQualityRange();
        }
    }
}
