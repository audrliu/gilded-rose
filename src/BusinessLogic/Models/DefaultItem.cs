﻿using csharpcore;

namespace GildedRose.BusinessLogic.Models
{
    public class DefaultItem : Item, IItem
    {
        protected const int MaxQuality = 50;
        protected const int MinQuality = 0;

        public new string Name { get; protected set; }
        public new int SellIn { get; protected set; }
        public new int Quality { get; protected set; }
        
        public DefaultItem(string name, int sellIn, int quality) 
        {
            Name = name;
            SellIn = sellIn;
            Quality = quality;

            ValidateAndFixQualityRange();
        }

        public virtual void Update()
        {
            SellIn--;

            if (Quality == 0)
            {
                // can't drop any more. We are done here.
                return;
            }

            if (SellIn >= 0)
            {
                Quality--;
            }
            else
            {
                // Once the sell by date has passed, Quality degrades twice as fast
                Quality -= 2;
            }

            ValidateAndFixQualityRange();
        }

        protected void ValidateAndFixQualityRange() 
        {
            if (Quality > MaxQuality)
            {
                //The Quality of an item is never more than 50
                Quality = MaxQuality;
            }

            if (Quality < MinQuality)
            {
                //The Quality of an item is never more than 50
                Quality = MinQuality;
            }
        }
    }
}
