﻿namespace GildedRose.BusinessLogic.Models
{
    public class ConjuredItem : DefaultItem
    {
        public ConjuredItem(string name, int sellIn, int quality) : base(name, sellIn, quality)
        {
        }

        public override void Update() 
        {
            SellIn--;

            if (Quality == 0)
            {
                // can't drop any more. We are done here.
                return;
            }

            if (SellIn >= 0)
            {
                Quality -= 2;
            }
            else
            {
                // Once the sell by date has passed, Quality degrades twice as fast
                Quality -= 4;
            }

            ValidateAndFixQualityRange();
        }
    }
}
