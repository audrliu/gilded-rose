﻿namespace GildedRose.BusinessLogic.Models
{
    public class BackstagePassesItem : DefaultItem
    {
        public BackstagePassesItem(string name, int sellIn, int quality) : base(name, sellIn, quality)
        {
        }

        public override void Update()
        {
            if (Quality == MaxQuality && SellIn > 0)
            {
                // Can't get better any more. We are done here.
                SellIn--;
                return;
            }

            if (SellIn <= 0)
            {
                // Quality drops to 0 after the concert
                SellIn--;
                Quality = 0;
                return;
            }

            //Quality increases by 2 when there are 10 days or less and by 3 when there are 5 days or less
            if (SellIn >= 0)
            {
                Quality++;
                if (SellIn <= 10)
                {
                    Quality++;
                }
                if (SellIn <= 5)
                {
                    Quality++;
                }
            }
            
            SellIn--;

            ValidateAndFixQualityRange();
        }
    }
}
