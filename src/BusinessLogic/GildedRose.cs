﻿using GildedRose.BusinessLogic.Models;
using System.Collections.Generic;

namespace GildedRose.BusinessLogic
{
    // todo. Class could be transformed to a service. Service should be initialized with DI
    public class GildedRose
    {
        private IList<IItem> items;

        public GildedRose(IList<IItem> Items)
        {
            this.items = Items;
        }

        public void UpdateQuality()
        {
            foreach (var item in items) 
            {
                item.Update();
            }
        }
    }
}
