﻿using csharpcore;
using GildedRose.BusinessLogic.Models;
using System;
using System.Collections.Generic;

namespace GildedRose.App
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("OMGHAI!");

            IList<IItem> Items = new List<IItem>{
                new DefaultItem("+5 Dexterity Vest", sellIn:10, quality:20),
                new AgedBrieItem("Aged Brie", 2, 0),
                new DefaultItem("Elixir of the Mongoose", 5, 7),
                new SulfurasItem("Sulfuras, Hand of Ragnaros", sellIn:0),
                new SulfurasItem("Sulfuras, Hand of Ragnaros", -1),
                new BackstagePassesItem("Backstage passes to a TAFKAL80ETC concert", sellIn:15, quality:20),
                new BackstagePassesItem("Backstage passes to a TAFKAL80ETC concert", 10, 49),
                new BackstagePassesItem("Backstage passes to a TAFKAL80ETC concert", 5, 49),
				new ConjuredItem("Conjured Mana Cake", sellIn:3, quality:6)
            };

            var app = new BusinessLogic.GildedRose(Items);

            for (var i = 0; i < 31; i++)
            {
                Console.WriteLine("-------- day " + i + " --------");
                Console.WriteLine("name, sellIn, quality");
                for (var j = 0; j < Items.Count; j++)
                {
                    Console.WriteLine(Items[j].Name + ", " + Items[j].SellIn + ", " + Items[j].Quality);
                }
                Console.WriteLine("");
                app.UpdateQuality();
            }
        }
    }
}
