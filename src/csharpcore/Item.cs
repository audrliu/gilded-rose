﻿namespace csharpcore
{
    public class Item
    {
        public static string Name { get; set; }
        public static int SellIn { get; set; }
        public static int Quality { get; set; }
    }
}
