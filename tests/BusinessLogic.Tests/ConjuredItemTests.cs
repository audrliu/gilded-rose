﻿using GildedRose.BusinessLogic.Models;
using Xunit;

namespace GildedRose.BusinessLogic.Tests
{
    public class ConjuredItemTests
    {
        [Fact]
        public void DegradedItemShouldNotAlterItsName()
        {
            var item = new ConjuredItem("Conjured Item Test", sellIn: 10, quality: 10);
            item.Update();

            Assert.Equal("Conjured Item Test", item.Name);
        }

        [Fact]
        public void ShouldDegradeInQualityByOneEachDay()
        {
            var item = new ConjuredItem("Conjured", sellIn: 10, quality: 10);
            item.Update();

            Assert.Equal(9, item.SellIn);
            Assert.Equal(8, item.Quality);
        }

        [Fact]
        public void ShouldDegradeInQualityTwiceAsFastIfSellInDateIsMissed()
        {
            var item = new ConjuredItem("Conjured", sellIn: 0, quality: 10);
            item.Update();

            Assert.Equal(-1, item.SellIn);
            Assert.Equal(6, item.Quality);
        }

        [Fact]
        public void QualityShouldNotBeNegative()
        {
            var item = new ConjuredItem("Conjured", sellIn: 50, quality: 2);
            for (int i = 0; i < 100; i++)
            {
                // Aging the item by 100 days
                item.Update();
            }

            Assert.Equal(-50, item.SellIn);
            Assert.Equal(0, item.Quality);
        }
    }
}
