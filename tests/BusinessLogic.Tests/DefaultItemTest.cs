﻿using GildedRose.BusinessLogic.Models;
using Xunit;

namespace GildedRose.BusinessLogic.Tests
{
    public class DefaultItemTest
    {
        [Fact]
        public void DegradedItemShouldNotAlterItsName()
        {
            var item = new DefaultItem("Default Item Test", sellIn: 5, quality: 5);
            item.Update();

            Assert.Equal("Default Item Test", item.Name);
        }

        [Fact]
        public void ShouldDegradeInQualityByOneEachDay()
        {
            var item = new DefaultItem("Default", sellIn: 10, quality: 10);
            item.Update();

            Assert.Equal(9, item.SellIn);
            Assert.Equal(9, item.Quality);
        }

        [Fact]
        public void ShouldDegradeInQualityTwiceAsFastIfSellInDateIsMissed()
        {
            var item = new DefaultItem("Default", sellIn: 0, quality: 10);
            item.Update();

            Assert.Equal(-1, item.SellIn);
            Assert.Equal(8, item.Quality);
        }

        [Fact]
        public void QualityShouldNotBeNegative()
        {
            var item = new DefaultItem("Default", sellIn: 50, quality: 2);
            for(int i = 0; i < 100; i++)
            {
                // Aging the item by 100 days
                item.Update();
            }
            
            Assert.Equal(-50, item.SellIn);
            Assert.Equal(0, item.Quality);
        }    
    }
}